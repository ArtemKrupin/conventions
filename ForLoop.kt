  class DateRange(val start: MyDate, val end: MyDate):Iterable<MyDate>{
  override operator fun iterator(): Iterator<MyDate> {
      val list=arrayListOf<MyDate>()
      var day=this.start
     
      do{
          list.add(day)
         day= day.followingDate()
      }while(day<=this.end)
      return list.iterator()
  }
}

fun iterateOverDateRange(firstDate: MyDate, secondDate: MyDate, handler: (MyDate) -> Unit) {
    for (date in firstDate..secondDate) {
          if(firstDate>=secondDate){
              break
          }
        handler(date)
  }
}
