class DateRange(val start: MyDate, val endInclusive: MyDate){
    fun contains(d: MyDate):Boolean{
        return this.start<d && d<=this.endInclusive
    }
}

fun checkInRange(date: MyDate, first: MyDate, last: MyDate): Boolean {
    return  DateRange(first, last).contains(date)
}
